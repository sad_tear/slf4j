#SLF4J 用户手册
简单的Java日志（SLF4J）用作各种日志记录框架的简单外观或抽象，例如java.util.logging，logback和log4j。 SLF4J允许最终用户在部署时插入所需的日志框架。 注意，启用你的库/应用程序的SLF4J意味着只添加一个强制依赖，即slf4j-api-1.7.21.jar

`SINCE 1.6.0` 如果在类路径上没有找到绑定，则SLF4J将默认为无操作实现。

`SINCE 1.7.0` Logger界面中的打印方法现在提供了接受[varargs](http://docs.oracle.com/javase/1.5.0/docs/guide/language/varargs.html)而不是Object []的变体。 此更改意味着SLF4J需要JDK 1.5或更高版本。 在内部，Java编译器将方法中的varargs部分转换为Object []。 因此，编译器生成的Logger接口在1.6.x中与1.6.x对等体无法区分。 因此，SLF4J版本1.7.x与SLF4J版本1.6.x完全100％不兼容。

`SINCE 1.7.5` 日志记录器检索时间的显着改进。 鉴于改进的程度，强烈建议用户迁移到SLF4J 1.7.5或更高版本。

`SINCE 1.7.9` 通过将`slf4j.detectLoggerNameMismatch`系统属性设置为true，SLF4J可以自动[找到错误命名的日志记录器](http://www.slf4j.org/codes.html#loggerNameMismatch)。

##Hello World

以传统通编程方式，这里是一个例子，说明使用SLF4J输出“Hello world”的最简单的方法。 它首先获取一个名为“HelloWorld”的日志记录器。 这个记录器反过来用于记录消息“Hello World”
```
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloWorld {
  public static void main(String[] args) {
    Logger logger = LoggerFactory.getLogger(HelloWorld.class);
    logger.info("Hello World");
  }
}
```

要运行这个例子，首先需要下载slf4j发行版，然后解压它。 一旦完成，添加文件slf4j-api-1.7.21.jar到你的类路径。
编译和运行HelloWorld将导致在控制台上打印以下输出。
```
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
```
只要向类路径添加绑定，警告就会消失。 假设你添加了slf4j-simple-1.7.21.jar，这样你的类路径包含：
- slf4j-api-1.7.21.jar
- slf4j-simple-1.7.21.jar

编译和运行Hello World在控制台上产生以下输出。
```
0 [main] INFO HelloWorld - Hello World
```
##Typical usage pattern(典型的使用模式)

下面的示例代码说明了SLF4J的典型使用模式。 请注意在第15行使用{} - placeholder。请参见问题“[What is the fastest way of logging?](http://www.slf4j.org/faq.html#logging_performance)" 在FAQ的更多详细信息。
```
 1: import org.slf4j.Logger;
 2: import org.slf4j.LoggerFactory;
 3:
 4: public class Wombat {
 5:  
 6:   final Logger logger = LoggerFactory.getLogger(Wombat.class);
 7:   Integer t;
 8:   Integer oldT;
 9:
10:   public void setTemperature(Integer temperature) {
11:    
12:     oldT = t;        
13:     t = temperature;
14:
15:     logger.debug("Temperature set to {}. Old temperature was {}.", t, oldT);
16:
17:     if(temperature.intValue() > 50) {
18:       logger.info("Temperature has risen above 50 degrees.");
19:     }
20:   }
21: }
```
##在部署时绑定与日志框架

如前所述，SLF4J支持各种日志框架。 SLF4J分发具有称为“SLF4J绑定”的多个jar文件，每个绑定对应于支持的框架。

---
***slf4j-log4j12-1.7.21.jar***

绑定log4j版本1.2，一个广泛使用的日志框架。 你还需要在你的类路径中放置log4j.jar。

---

***slf4j-jdk14-1.7.21.jar***

java.util.logging的绑定，也称为JDK 1.4日志记录

---

***slf4j-nop-1.7.21.jar***

绑定NOP，静默丢弃所有日志记录。

---

***slf4j-simple-1.7.21.jar***

绑定简单实现，将所有事件输出到System.err。 只打印级别INFO及更高级别的消息。 这种绑定在小应用程序的上下文中可能是有用的。

---

***slf4j-jcl-1.7.21.jar***

Jakarta Commons Logging的绑定。 此绑定将所有SLF4J日志记录委派给JCL。

---

***logback-classic-1.0.13.jar (requires logback-core-1.0.13.jar)***

本地实现在SLF4J项目外部也有SLF4J绑定，例如。 logback实现SLF4J本地。 Logback的ch.qos.logback.classic.Logger类是SLF4J的org.slf4j.Logger接口的直接实现。 因此，使用SLF4J结合logback涉及严格零内存和计算开销。

---
要切换日志框架，只需替换类路径上的slf4j绑定。 例如，要从java.util.logging切换到log4j，只需将slf4j-jdk14-1.7.21.jar替换为slf4j-log4j12-1.7.21.jar。

SLF4J不依赖于任何特殊的类装载机械。 事实上，每个SLF4J绑定在编译时都是硬连线的，只使用一个特定的日志框架。 例如，slf4j-log4j12-1.7.21.jar绑定在编译时绑定为使用log4j。 在你的代码中，除了slf4j-api-1.7.21.jar，你只需要将一个并且只有一个绑定放到适当的类路径位置。 不要在类路径中放置多个绑定。 以下一个概念的图形说明。

![SLF4J模型](images/concrete-bindings.png)

SLF4J接口及其各种适配器极为简单。 大多数熟悉Java语言的开发人员应该能够在不到一小时内阅读和完全理解代码。 不需要类加载器的知识，因为SLF4J不使用，也不直接访问任何类加载器。 因此，SLF4J没有遇到Jakarta Commons Logging（JCL）观察到的类加载器问题或内存泄漏。

鉴于SLF4J接口及其部署模型的简单性，新的日志框架的开发人员应该很容易编写SLF4J绑定。

##Libraries

广泛分布的组件和库的作者可以对SLF4J接口进行编码，以避免在其最终用户上强加日志框架。 因此，最终用户可以在部署时通过在类路径上插入相应的slf4j绑定来选择期望的日志框架，其可以稍后通过在类路径上用另一个替换现有绑定并重新启动应用来改变。 这种方法已被证明是简单和非常健壮的。

**从 SLF4J version 1.6.0开始**,
如果在类路径上找不到绑定，则slf4j-api将默认为放弃所有日志请求的无操作实现。 因此，不是抛出一个NoClassDefFoundError，因为org.slf4j.impl.StaticLoggerBinder类丢失，SLF4J版本1.6.0和更高版本将发出一个关于缺少绑定的警告消息，并继续丢弃所有日志请求，没有进一步的抗议。 例如，让Wombat是一些生物学相关的框架，取决于SLF4J用于日志记录。 为了避免对最终用户强加日志框架，Wombat的分布包括slf4j-api.jar，但没有绑定。 即使在类路径上没有任何SLF4J绑定，Wombat的分发仍然可以开箱即用，并且不需要最终用户从SLF4J的网站下载绑定。 只有当最终用户决定启用日志记录时，她才需要安装与她选择的日志框架相对应的SLF4J绑定。

**基本规则嵌入式组件（如库或框架）不应声明对任何SLF4J绑定的依赖，而只依赖于slf4j-api。** 当库声明对特定绑定的传递依赖性时，该绑定强加给最终用户否定SLF4J的目的。 注意，声明对绑定的非传递依赖性，例如用于测试，不影响最终用户。
SLF4J在嵌入式组件中的使用也在FAQ中与 [logging configuration](http://www.slf4j.org/faq.html#configure_logging), [dependency reduction](http://www.slf4j.org/faq.html#optional_dependency) and [testing](http://www.slf4j.org/faq.html#optional_dependency)。

## 声明用于日志记录的项目依赖关系

给定Maven传递依赖性规则，对于“常规”项目（不是库或框架），声明日志记录依赖关系可以使用单个依赖性声明来实现。

**`LOGBACK-CLASSIC`** 如果你希望使用logback-classic作为底层日志框架，你所需要做的就是在你的pom.xml文件中声明“`ch.qos.logback:logback-classic`”作为依赖关系，如下所示。 除了`logback-classic-1.0.13.jar`，这将拉`slf4j-api-1.7.21.jar`以及`logback-core-1.0.13.jar`到你的项目中。 注意，明确声明对`logback-core-1.0.1`3或`slf4j-api-1.7.21.jar`的依赖不是错误的，可能需要通过Maven的“最近定义”依赖调解规则强加所述artifacts的正确版本 。
```
<dependency>
  <groupId>ch.qos.logback</groupId>
  <artifactId>logback-classic</artifactId>
  <version>1.0.13</version>
</dependency>
```

**`LOG4J`** 如果你希望使用log4j作为底层日志框架，你所需要做的就是在你的pom.xml文件中声明“`org.slf4j:slf4j-log4j12`”作为依赖关系，如下所示。 除了`slf4j-log4j12-1.7.21.jar`，这将拉`slf4j-api-1.7.21.jar`以及`log4j-1.2.17.jar`到你的项目中。 请注意，显式声明对`log4j-1.2.17.jar`或`slf4j-api-1.7.21.jar`的依赖不是错误的，可能需要通过Maven的“最近定义”依赖调解规则强加所述artifacts的正确版本 。
```
<dependency>
  <groupId>org.slf4j</groupId>
  <artifactId>slf4j-log4j12</artifactId>
  <version>1.7.21</version>
</dependency>
```
**`JAVA.UTIL.LOGGING`** 如果你想使用`java.util.logging`作为底层日志框架，你所需要做的就是在你的pom.xml文件中声明“`org.slf4j:slf4j-jdk14`”作为依赖关系，如下所示。 除了`slf4j-jdk14-1.7.21.jar`，这将拉`slf4j-api-1.7.21.jar`到您的项目。 请注意，显式声明对`slf4j-api-1.7.21.jar`的依赖不是错误的，可能需要通过Maven的“最近定义”依赖调解规则强加所述artifacts的正确版本 。
```
<dependency>
  <groupId>org.slf4j</groupId>
  <artifactId>slf4j-jdk14</artifactId>
  <version>1.7.21</version>
</dependency>
```
##Binary compatibility(二进制兼容性)

SLF4J绑定指定了一个artifacts，如`slf4j-jdk14.jar`或`slf4j-log4j12.jar`，用于将slf4j绑定到底层日志记录框架（例如`java.util.logging`和`log4j`）。

混合不同版本的`slf4j-api.jar`和SLF4J绑定可能会导致问题。 例如，如果你使用`slf4j-api-1.7.21.jar`，那么你也应该使用`slf4j-simple-1.7.21.jar`，使用`slf4j-simple-1.5.5.jar`不会工作。

然而，从客户的角度来看，所有版本的slf4j-api都是兼容的。 使用`slf4j-api-N.jar`编译的客户端代码将与`slf4j-api-M.jar`一起运行，对于任何N和M，您只需要确保绑定的版本与`slf4j-api.jar`的版本匹配。 您不必担心项目中给定依赖关系使用的`slf4j-api.jar`的版本。 你可以随时使用任何版本的`slf4j-api.jar`，并且只要版本的`slf4j-api.jar`和它的绑定匹配，你应该就好了。

在初始化时，如果SLF4J怀疑可能存在slf4j-api与绑定版本不匹配问题，它将发出关于可疑不匹配的警告。
##Consolidate logging via SLF4J(通过SLF4J合并日志记录)
通常，给定项目将取决于依赖于除SLF4J之外的日志API的各种组件。 通常根据JCL，java.util.logging，log4j和SLF4J的组合来查找项目。 然后希望通过单个通道合并日志记录。 SLF4J通过为JCL，java.util.logging和log4j提供桥接模块来满足这种常见用法。 有关详细信息，请参阅有关[桥接旧API](http://www.slf4j.org/legacy.html)的页面。

##映射诊断上下文（MDC）支持

“映射诊断上下文”本质上是由日志框架维护的映射，其中应用程序代码提供键值对，然后可以由日志框架将其插入日志消息中。 MDC数据也可以在过滤消息或触发某些操作方面非常有用。

SLF4J支持MDC或映射的诊断上下文。 如果底层日志框架提供MDC功能，则SLF4J将委托给底层框架的MDC。 注意，此时，只有log4j和logback提供MDC功能。 如果底层框架不提供MDC，例如java.util.logging，则SLF4J仍然存储MDC数据，但是其中的信息将需要由自定义用户代码检索。

因此，作为SLF4J用户，您可以在log4j 或 logback的环境下利用MDC信息，但不会强制这些日志框架作为依赖关系。

有关MDC的更多信息，请参阅logback手册中的[MDC](http://logback.qos.ch/manual/mdc.html)一章。

##Executive summary(执行摘要)

| 优点 | 描述 |
|-----------| --------- |
| 在部署时选择您的日志框架 | 所需的日志框架可以在部署时通过在类路径中插入适当的jar文件（绑定）来插入。 |
| 故障快速操作 | 由于JVM加载类的方式，框架绑定将很早被自动验证。 如果SLF4J在类路径上找不到绑定，它将发出单个警告消息，并且默认为无操作实现。 |
| 流行的日志框架的绑定 | SLF4J支持流行的日志框架，即log4j，java.util.logging，简单日志和NOP。 logback项目本身支持SLF4J。 |
| 桥接遗留日志API | 在SLF4J上实现JCL，即jcl-over-slf4j.jar，将允许您的项目部分迁移到SLF4J，而不会破坏与使用JCL的现有软件的兼容性。 类似地，log4j-over-slf4j.jar和jul-to-slf4j模块将允许您将log4j和java.util.logging调用重定向到SLF4J。 有关详细信息，请参阅有关[桥接旧API](http://www.slf4j.org/legacy.html)的页面。 |
| 迁移源代码 | slf4j-migrator实用程序可以帮助您迁移源以使用SLF4J。 |
| 支持参数化日志消息 | 所有SLF4J绑定都支持带有[显着改进的性能](http://www.slf4j.org/faq.html#logging_performance)结果的参数化日志消息。 |
