#FAQ

## Frequently Asked Questions about SLF4J(有关SLF4J的常见问题)

###Generalities

1. What is SLF4J?
2. When should SLF4J be used?
3. Is SLF4J yet another loggingfacade?
4. If SLF4J fixes JCL, then why wasn't the fix made in JCL instead of creating a new project?
5. When using SLF4J, do I have to recompile my application to switch to a different logging system?
6. What are SLF4J's requirements?
7. Are SLF4J versions backward compatible?
8. I am getting IllegalAccessError exceptions when using SLF4J. Why is that?
9. Why is SLF4J licensed under X11 type license instead of the Apache Software License?
10. Where can I get a particular SLF4J binding?
11. Should my library attempt to configure logging?
12. In order to reduce the number of dependencies of our software we would like to make SLF4J an optional dependency. Is that a good idea?
13. What about Maven transitive dependencies?
14. How do I exclude commons-logging as a Maven dependency?

###About the SLF4J API

1. Why don't the printing methods in the Logger interface accept message of type Object, but only messages of type String?
2. Can I log an exception without an accompanying message?
What is the fastest way of (not) logging?
3. How can I log the string contents of a single (possibly complex) object?
4. Why doesn't the org.slf4j.Logger interface have methods for the FATAL level?
5. Why was the TRACE level introduced only in SLF4J version 1.4.0?
6. Does the SLF4J logging API support I18N (internationalization)?
7. Is it possible to retrieve loggers without going through the static methods in LoggerFactory?
8. In the presence of an exception/throwable, is it possible to parameterize a logging statement?

###Implementing the SLF4J API

1. How do I make my logging framework SLF4J compatible?
2. How can my logging system add support for the Marker interface?
3. How does SLF4J's version check mechanism work?

###General questions about logging

1. Should Logger members of a class be declared as static?
2. Is there a recommended idiom for declaring a loggers in a class?

---

##Generalities

###What is SLF4J?

SLF4J是记录系统的简单接口，允许最终用户在部署时插入所需的日志系统。

###When should SLF4J be used?

简而言之，库和其他嵌入式组件应该考虑SLF4J的日志记录需求，因为库不能强加他们在最终用户上选择日志框架。 另一方面，对于独立应用程序使用SLF4J并不一定有意义。 独立应用程序可以直接调用他们选择的日志框架。 在logback的情况下，问题是moot，因为logback通过SLF4J暴露其logger API。

SLF4J只是一个外观，意味着它不提供一个完整的日志解决方案。 使用SLF4J无法执行诸如配置追加器或设置记录级别等操作。 因此，在某个时间点，任何值得重视的应用程序将需要直接调用底层日志系统。 换句话说，对于独立应用程序，完全独立于API底层日志记录系统是不可能的。 然而，SLF4J减少了这种依赖对移植的影响。

假设您的CRM应用程序使用log4j进行日志记录。 但是，您的一个重要客户端请求通过JDK 1.4日志记录执行日志记录。 如果你的应用程序充斥着成千上万的直接log4j调用，迁移到JDK 1.4将是一个相对冗长和容易出错的过程。 更糟糕的是，您可能需要维护两个版本的CRM软件。 如果你一直调用SLF4J API而不是log4j，迁移可以在几分钟内完成，只需将一个jar文件替换为另一个。

SLF4J让我们的组件开发人员将日志系统的选择推迟到最终用户，但最终需要做出选择。

###Is SLF4J yet another logging facade?

SLF4J在概念上与JCL非常相似。 因此，它可以被认为是另一个日志门面。 然而，SLF4J在设计上简单得多，并且可以说更稳健。 简而言之，SLF4J避免了JCL遇到的类加载器问题。

###If SLF4J fixes JCL, then why wasn't the fix made in JCL instead of creating a new project?

这是一个很好的问题。 首先，SLF4J的静态绑定方法非常简单，甚至可笑的是这样。 要让开发者相信这种方法的有效性并不容易。 只有在SLF4J被发布并开始被接受后，它在相关社区获得尊重。

###When using SLF4J, do I have to recompile my application to switch to a different logging system?

不，您不需要重新编译您的应用程序。 您可以通过删除先前的SLF4J绑定并将其替换为您选择的绑定来切换到其他日志记录系统。

例如，如果您使用NOP实现，并且想要切换到log4j版本1.2，只需在类路径中将slf4j-nop.jar替换为slf4j-log4j12.jar，但不要忘记添加log4j-1.2.x.jar 以及。 想要切换到JDK 1.4日志记录？ 只需将slf4j-log4j12.jar替换为slf4j-jdk14.jar即可。

###What are SLF4J's requirements?

从版本1.7.0开始，SLF4J需要JDK 1.5或更高版本。 早期的SLF4J版本，即SLF4J 1.4,1.5。 和1.6，需要JDK 1.4。
| Binding         |	Requirements |
|---------------- |------------- |
| slf4j-nop       | JDK 1.5 |
| slf4j-simple	  | JDK 1.5 |
| slf4j-log4j12	  | JDK 1.5, plus any other library dependencies required by the log4j appenders in use |
| slf4j-jdk14     | JDK 1.5 or later |
| logback-classic | JDK 1.5 or later, plus any other library dependencies required by the logback appenders in use |

###Are SLF4J versions backward compatible?(SLF4J版本是否向下兼容)

从客户的角度来看，SLF4J API向后兼容所有版本。 这意味着，您可以从SLF4J版本1.0升级到任何更高版本没有问题。 使用slf4j-api-versionN.jar编译的代码将与任何versionN和任何versionM的slf4j-api-versionM.jar一起使用。**到目前为止，slf4j-api中的二进制兼容性从未被破坏。**

然而，虽然SLF4J API从客户端的角度来看非常稳定，但SLF4J绑定 slf4j-simple.jar或slf4j-log4j12.jar，可能需要特定版本的slf4j-api。 混合不同版本的slf4j artifacts 可能会有问题，因此强烈建议不要使用。 例如，如果你使用slf4j-api-1.5.6.jar，那么你也应该使用slf4j-simple-1.5.6.jar，使用slf4j-simple-1.4.2.jar将不会工作。

在初始化时，如果SLF4J怀疑可能存在版本不匹配问题，它会发出关于所述不匹配的警告。

###I am getting IllegalAccessError exceptions when using SLF4J. Why is that?

这是异常详情
```
Exception in thread "main" java.lang.IllegalAccessError: tried to access field
org.slf4j.impl.StaticLoggerBinder.SINGLETON from class org.slf4j.LoggerFactory
   at org.slf4j.LoggerFactory.<clinit>(LoggerFactory.java:60)
```
此错误是由LoggerFactory类的静态初始化程序尝试直接访问*org.slf4j.impl.StaticLoggerBinder*的SINGLETON字段引起的。 虽然这在SLF4J 1.5.5和更早版本中允许，在1.5.6和更高版本中，SINGLETON字段已标记为私有访问。


如果你得到上面显示的异常，那么你使用的是slf4j-api的旧版本。 1.4.3，具有新版本的slf4j结合，例如 1.5.6。 通常，当你的Maven pom.ml文件包含了声明对slf4j-api版本1.4.2的依赖的hibernate 3.3.0时，会发生这种情况。 如果你的pom.xml声明一个依赖于slf4j绑定，说slf4j-log4j12版本1.5.6，那么你会得到非法访问错误。

要查看哪个版本的slf4j-api是由Maven拉入，使用maven依赖插件如下。
```
mvn dependency:tree
```
如果使用Eclipse，请不要依赖m2eclipse显示的依赖关系树。

在你的pom.xml文件中，显式声明一个依赖slf4j-api匹配的声明绑定的版本将使问题消失。

###Why is SLF4J licensed under X11 type license instead of the Apache Software License?(为什么SLF4J按照X11类型许可证而不是Apache软件许可证授权？)

SLF4J按照允许的X11类型许可证而不是ASL或LGPL许可，因为Apache软件基金会和自由软件基金会认为X11许可证与其各自的许可证兼容。

###Where can I get a particular SLF4J binding?(在哪里可以获得特定的SLF4J绑定？)

用于`SimpleLogger`，`NOPLogger`，`Log4jLoggerAdapter`和`JDK14LoggerAdapter`的SLF4J绑定包含在文件`slf4j-nop.jar`，`slf4j-simple.jar`，`slf4j-log4j12.jar`和`slf4j-jdk14.jar`中。 这些文件随官方SLF4J发行版一起提供。 请注意，所有绑定依赖于`slf4j-api.jar`。

`logback-classic`的绑定及其logback分支。 但是，与所有其他绑定一样，`logback-classic`绑定需要`slf4j-api.jar`。

###Should my library attempt to configure logging?

**嵌入式组件如库不仅不需要配置底层的日志框架，他们真的不应该这样做。** 他们应该调用SLF4J进行日志记录，但应让最终用户配置日志记录环境。 当嵌入式组件尝试自己配置日志记录时，它们通常会覆盖最终用户的意愿。 一天结束时，最终用户必须读取日志并对其进行处理。 她应该是决定如何配置她的日志记录的人。

###In order to reduce the number of dependencies of our software we would like to make SLF4J an optional dependency. Is that a good idea?(为了减少我们软件的依赖关系的数量，我们希望使SLF4J成为可选的依赖关系。 这是个好主意吗？)

每当软件项目达到需要设计记录策略的点时，这个问题就会弹出。

让Wombat一个成为只有很少的依赖软件库。如果选择SLF4J作为Wombat的日志API，那么对slf4j-api.jar的新依赖将添加到Wombat的依赖关系列表中。考虑到编写日志封装似乎并不困难，有些开发人员会试图封装SLF4J，并且只有在它已经存在于类路径上时才会链接它，使SLF4J成为Wombat的可选依赖项。除了解决依赖性问题，包装器将隔离Wombat和SLF4J的API，确保Wombat中的日志记录是面向未来的。

另一方面，任何SLF4J包装器根据定义依赖于SLF4J。它必须有相同的一般API。如果将来一个新的和显着不同的日志API出现，使用包装器的代码将同样难以迁移到新的API作为直接使用SLF4J的代码。因此，包装器不太可能防止你的代码，但通过在SLF4J的顶部添加一个额外的间接层，间接层使它更复杂。

**`增加的脆弱性`** 它实际上比那更糟糕。封装器将需要依赖于某些内部SLF4J接口，这些接口随时间而变化，这与从不改变的面向客户端的API相反。因此，包装器通常取决于它们编译的主要版本。针对SLF4J版本1.5.x编译的包装器不能与SLF4J 1.6一起使用，而使用org.slf4j.Logger，LoggerFactory，MarkerFactory，org.slf4j.Marker和MDC的客户端代码将适用于版本1.0及更高版本的任何SLF4J版本。

有理由假设在大多数项目中，Wombat将是许多人中的一个依赖。如果每个库都有自己的日志记录封装器，那么每个封装器可能需要单独配置。因此，不需要处理一个日志框架，即SLF4J，Wombat的用户也必须详细描述Wombat的日志封装。每个框架会出现自己的包装，以使SLF4J可选，这个问题会更加复杂。 （配置或处理五个不同的日志封装器的复杂性不是令人兴奋或不令人愉快的。）

Velocity项目采用的记录策略是“定制记录抽象”反模式的一个很好的例子。通过采用独立的日志抽象策略，Velocity开发人员使自己的生活更加困难，但更重要的是，他们使得用户的生活更加困难。

一些项目尝试检测类路径上SLF4J的存在，并切换到它（如果存在）。虽然这种方法看起来足够透明，但会导致错误的位置信息。底层日志框架将打印包装器的位置（类名和行号），而不是真正的调用者。然后有一个API覆盖的问题，SLF4J支持MDC和标记，除了参数化日志记录。虽然人们可以在几个小时内想出一个看似工作的SLF4J包装，但是很多技术问题将随着时间的推移而出现，Wombat开发人员必须处理这些问题。请注意，SLF4J已经演变了几年，并有260个错误报告提出。

由于上述原因，框架的开发人员应该抵制编写自己的日志封装器的诱惑。它不仅浪费了开发人员的时间，它实际上使生活对于所述框架的用户更加困难，并且使得日志代码矛盾地更容易改变。

###What about Maven transitive dependencies?(Maven传递依赖关系呢？)

作为使用Maven构建的库的作者，您可能需要使用绑定（例如slf4j-log4j12或logback-classic）测试应用程序，而不强制使用log4j或logback-classic作为对用户的依赖。 这是相当容易完成。

由于您的库的代码依赖于SLF4J API，因此您需要将slf4j-api声明为编译时（默认范围）依赖关系。
```
<dependency>
  <groupId>org.slf4j</groupId>
  <artifactId>slf4j-api</artifactId>
  <version>1.7.21</version>
</dependency>
```
限制在测试中使用的SLF4J绑定的传递性可以通过将SLF4J绑定依赖关系的作用域声明为“test”来实现。 这里是一个例子：
```
<dependency>
  <groupId>org.slf4j</groupId>
  <artifactId>slf4j-log4j12</artifactId>
  <version>1.7.21</version>
  <scope>test</scope>
</dependency>
```
因此，就您的用户而言，您正在将slf4j-api导出为库的传递依赖项，但不导出任何SLF4J绑定或任何底层日志记录系统。

注意，从SLF4J版本1.6开始，在没有SLF4J绑定的情况下，slf4j-api将默认为无操作实现。

###How do I exclude commons-logging as a Maven dependency?(如何将commons-logging作为Maven依赖关系排除？)
####alternative 1) explicit exclusion(显式排除)
许多使用Maven的软件项目将commons-logging声明为依赖关系。 因此，如果您希望迁移到SLF4J或使用jcl-over-slf4j，则需要在所有项目的依赖项中排除commons-logging，这些依赖项依赖于commons-logging。 依赖性排除在Maven文档中描述。 排除对多个pom.xml文件上分布的多个依赖项显式的commons-logging可能是一个繁琐的，相对容易出错的过程。
####alternative 2) provided scope(提供范围)
通过在项目的pom.xml文件中的提供的范围中声明它，Commons-logging可以被相当简单和方便地排除为依赖关系。 实际的commons-logging类将由jcl-over-slf4j提供。 这转换为以下pom文件片段：
```
<dependency>
  <groupId>commons-logging</groupId>
  <artifactId>commons-logging</artifactId>
  <version>1.1.1</version>
  <scope>provided</scope>
</dependency>

<dependency>
  <groupId>org.slf4j</groupId>
  <artifactId>jcl-over-slf4j</artifactId>
  <version>1.7.21</version>
</dependency>
```
第一个依赖声明实质上声明公共日志记录将是“以某种方式”由您的环境提供。 第二个声明包括jcl-over-slf4j到你的项目中。 由于jcl-over-slf4j是一个完美的二进制兼容替代commons-logging，第一个断言变为true。

不幸的是，当在提供的作用域中声明commons-logging获取作业时，你的IDE，例如。 Eclipse，仍然会将commons-logging.jar放在项目的类路径中，如IDE所示。 您需要确保jcl-over-slf4j.jar在您的IDE的commons-logging.jar之前可见。

####alternative 3) empty artifacts(空的artifacts)
另一种方法是依赖于一个空的commons-logging.jar工件。 这种聪明的方法首先是想象的，最初由Erik van Oosten支持。

空的artifacts可从http://version99.qos.ch高可用性Maven存储库获得，在位于不同地理区域的多个主机上复制。

以下声明将version99存储库添加到由Maven搜索的一组远程存储库。 此存储库包含commons-logging和log4j的空工件。 顺便说一句，如果你使用version99版本库，请在<version99 AT qos.ch>下拉一行。
```
<repositories>
  <repository>
    <id>version99</id>
    <!-- highly available repository serving empty artifacts -->
    <url>http://version99.qos.ch/</url>
  </repository>
</repositories>
```
在项目的 *<dependencyManagement>* 部分中声明版本99-空的commons-logging将引导commons-logging的所有传递依赖性导入版本99为空，因此很好地解决了commons-logging排除问题。 commons-logging的类将由jcl-over-slf4j提供。 以下行声明commons-logging版本99为空（在依赖关系管理部分中），并将jcl-over-slf4j声明为依赖关系。
```
<dependencyManagement>
  <dependencies>
    <dependency>
      <groupId>commons-logging</groupId>
      <artifactId>commons-logging</artifactId>
      <version>99-empty</version>
    </dependency>
    ... other declarations...
  </dependencies>
</dependencyManagement>

<!-- Do not forget to declare a dependency on jcl-over-slf4j in the        -->
<!-- dependencies section. Note that the dependency on commons-logging     -->
<!-- will be imported transitively. You don't have to declare it yourself. -->
<dependencies>
  <dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>jcl-over-slf4j</artifactId>
    <version>1.7.21</version>
  </dependency>
  ... other dependency declarations
</dependencies>
```
##About the SLF4J API
###Why don't the printing methods in the Logger interface accept message of type Object, but only messages of type String?(为什么在Logger接口中的打印方法不接受类型为Object的消息，但只接受类型为String的消息？)
在SLF4J 1.0beta4中，修改了Logger界面中的打印方法，如debug（），info（），warn（），error（），以便仅接受String类型而不是Object类型的消息。

因此，DEBUG级别的打印方法集为：
```
debug(String msg);
debug(String format, Object arg);
debug(String format, Object arg1, Object arg2);           
debug(String msg, Throwable t);
```
以前，上述方法中的第一个参数的类型为Object。

这种改变强制了日志系统关于装饰和处理类型为String的消息，而不是任何任意类型（对象）的概念。

同样重要的是，新的方法签名集提供了重载方法之间更清晰的区分，而以前由于Java重载规则的调用方法的选择并不总是容易遵循。

这也很容易犯错误。 例如，以前写法是合法的：
```
logger.debug(new Exception("some error"));
```
不幸的是，上面的调用没有打印异常的堆栈跟踪。 因此，可能丢失潜在的关键信息。 当第一个参数被限制为String类型时，只有方法
```
debug(String msg, Throwable t);
```
可用于记录异常。 请注意，此方法确保每个记录的异常都伴随着描述性消息。
###Can I log an exception without an accompanying message?(我可以记录一个异常而没有伴随的消息吗？)
总之，没有。

如果e是异常，并且您想在ERROR级别记录异常，则必须添加一条伴随的消息。 例如，
```
logger.error("some accompanying message", e);
```
你可能合理地认为，并非所有的异常都有一个有意义的信息陪伴他们。 此外，一个好的异常应该已经包含了一个自我解释的描述。 因此，伴随的消息可以被认为是冗余的。

虽然这些是有效的论据，但有三个相反的论证也值得考虑。 首先，在许多情况下，即使不是所有的情况，伴随的消息可以传达有用的信息，很好地补充了异常中包含的描述。 通常，在记录异常的点，开发者可以访问更多的上下文信息，而不是抛出异常的点。 第二，不难想象更多或更少的通用消息，例如。 “异常捕获”，“异常跟随”，可以用作错误（String msg，Throwable t）调用的第一个参数。 第三，大多数日志输出格式在一行上显示消息，随后在单独的行上显示异常。 因此，消息行看起来不一致，没有消息。

简而言之，如果用户被允许记录异常而没有伴随的消息，则日志系统将发明一个消息。 这实际上是java.util.logging包中的throwing（String sourceClass，String sourceMethod，Throwable thrown）方法。 （它自己决定伴随的消息是字符串“THROW”。）

最初可能需要一个附带的消息来记录异常。 然而，这是所有log4j派生系统中的常见做法，如java.util.logging，logkit等，当然还有log4j本身。 看来，当前的共识认为要求伴随的消息是一件好事（TM）。

###What is the fastest way of (not) logging?什么是最快的方式（不）记录？
SLF4J支持一种称为参数化日志记录的高级功能，可以显着提高禁用日志记录语句的日志记录性能。

对于一些Logger记录器，写，
```
logger.debug("Entry number: " + i + " is " + String.valueOf(entry[i]));
```
导致构造消息参数的成本，即将整数i和entry [i]转换为字符串，并且连接中间字符串。 这个，不管消息是否会被记录。

避免参数构造成本的一种可能方法是通过用测试来围绕日志语句。 这里是一个例子。
```
if(logger.isDebugEnabled()) {
  logger.debug("Entry number: " + i + " is " + String.valueOf(entry[i]));
}
```
这样，如果对记录器禁用调试，则不会产生参数构造的成本。 另一方面，如果为DEBUG级别启用了记录器，则将产生两次评估记录器是否启用的成本：一次在debugEnabled中，一次在调试中。 这是一个不显着的开销，因为评估日志记录器需要不到1％的时间实际记录语句。

**更好的是，使用参数化消息**

基于消息格式存在非常方便的替代方案。 假设条目是一个对象，您可以写：
```
Object entry = new SomeObject();
logger.debug("The entry is {}.", entry);
```
在评估是否记录之后，并且仅当判定是肯定的时，记录器实现格式化消息并且用条目的字符串值替换“{}”对。 换句话说，在禁用日志语句的情况下，此形式不会产生参数构造的成本。

以下两行将产生完全相同的输出。 但是，在禁用日志记录语句的情况下，第二种形式将优于第一种形式至少30倍。
```
logger.debug("The new entry is "+entry+".");
logger.debug("The new entry is {}.", entry);
```
还有两个参数变量。 例如，您可以写：
```
logger.debug("The new entry is {}. It replaces {}.", entry, oldEntry);
```
如果需要传递三个或更多参数，则可以使用打印方法的Object ...变体。 例如，您可以写：
```
logger.debug("Value {} was inserted between {} and {}.", newVal, below, above);
```
这种形式带来构造一个通常非常小的Object []（对象数组）的隐藏成本。 一个和两个参数变量不会产生这个隐藏的成本，并且仅仅因为这个原因（效率）而存在。 slf4j-api会更小/更清洁，只有Object ...变体。

还支持数组类型参数，包括多维数组。

SLF4J使用自己的消息格式化实现，它与Java平台的消息格式化实现不同。 这是因为SLF4J的执行速度提高了大约10倍，但是代价是非标准和不太灵活。

####转义“{}”对
“{}”对被称为格式化锚。 它用于指定在消息模式中需要替换参数的位置。

SLF4J只关心格式化锚，即'{'字符后面紧跟'}'。 因此，如果您的消息包含“{”或“}”字符，您不必做任何特殊的，除非“}”字符紧跟在“}”之后。 例如，
```
logger.debug("Set {1,2} differs from {}", "3");
```
将会打印"Set {1,2} differs from 3".

你可能曾经写过，
```
logger.debug("Set {1,2} differs from {{}}", "3");
```
将会打印"Set {1,2} differs from {3}".

在极少数情况下，“{}”对在您的文本中自然出现，并且您希望禁用格式化锚的特殊含义，那么您需要使用“\”转义“{”字符，即“\”字符。 只有“{”字符应该转义。 没有必要转义“}”字符。 例如，
```
logger.debug("Set \\{} differs from {}", "3");
```
将打印为“Set {} differs from 3”不同。 请注意，在Java代码中，反斜杠字符需要写为“\\”。
在极少数情况下，“\{}”自然地出现在消息中，您可以双重转义格式化锚，以便保留其原始含义。 例如，
```
logger.debug("File name is C:\\\\{}.", "file.zip");
```
将打印为 "File name is C:\file.zip".

###How can I log the string contents of a single (possibly complex) object?(如何记录单个（可能复杂）对象的字符串内容？)
在要记录的消息是对象的字符串形式的相对罕见的情况下，可以使用适当级别的参数化打印方法。 假设complexObject是某个复杂度的对象，对于级别DEBUG的日志语句，可以写为：
```
logger.debug("{}", complexObject);
```
日志系统只有在确定启用了日志语句之后才会调用complexObject.toString（）方法。 否则，将有利地避免complexObject.toString（）转换的成本。

###Why doesn't the org.slf4j.Logger interface have methods for the FATAL level?(为什么org.slf4j.Logger接口没有FATAL级别的方法？)
Marker接口是org.slf4j软件包的一部分，它使FATAL级别大大冗余。 如果给定的错误需要超出为普通错误分配的注意力，只需使用特别指定的标记（可以命名为“FATAL”或任何其他名称）将日志语句标记为您的喜好。

例:
```
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

class Bar {
  void foo() {
    Marker fatal = MarkerFactory.getMarker("FATAL");
    Logger logger = LoggerFactory.getLogger("aLogger");

    try {
      ... obtain a JDBC connection
    } catch (JDBException e) {
      logger.error(fatal, "Failed to obtain JDBC connection", e);
    }
  }
}
```
虽然标记是SLF4J API的一部分，但只有logback支持现成的标记。 例如，如果将％标记转换字添加到其模式中，logback的PatternLayout将在其输出中添加标记数据。 标记数据可用于[过滤邮件](http://logback.qos.ch/manual/filters.html)，甚至[在单个事务结束](http://logback.qos.ch/recipes/emailPerTransaction.html)时[触发](http://logback.qos.ch/manual/appenders.html#OnMarkerEvaluator)外发电子邮件。

与log4j和java.util.logging等不支持标记的日志框架结合使用时，标记数据将被忽略。

标记添加一个新维度，具有无限可能的值，用于处理日志语句，与级别允许的五个值（即ERROR，WARN，INFO，DEBUG和TRACE）相比。 目前，只有logback支持标记数据。 然而，没有什么能防止其他日志框架使用标记数据。

###Why was the TRACE level introduced only in SLF4J version 1.4.0?(为什么仅在SLF4J版本1.4.0中引入TRACE级别？)
添加TRACE级别是经常争论的。通过研究各种项目，我们观察到TRACE级别用于禁用某些类的日志输出，而无需为这些类配置日志记录。事实上，默认情况下，在log4j和logback以及大多数其他日志记录系统中禁用TRACE级别。通过在配置文件中添加相应的指令可以实现相同的结果。

因此，在许多情况下，TRACE级别携带与DEBUG相同的语义。在这种情况下，TRACE级别只保存一些配置指令。在其他更有趣的场合，TRACE携带与DEBUG不同的含义，Marker对象可以用来传达所需的含义。但是，如果您不能打扰标记并希望使用低于DEBUG的日志记录级别，则TRACE级别可以完成该作业。

请注意，虽然判断禁用的日志请求的成本是几纳秒的量级，但在严格循环中不鼓励使用TRACE级别（或任何其他级别），其中日志请求可能被评估数百万次。如果启用日志请求，那么它将以巨大的输出压垮目标。如果请求被禁用，它将浪费资源。

总之，虽然我们仍然不鼓励使用TRACE级别，因为替代品存在，或者因为在许多情况下，级别TRACE的日志请求是浪费的，因为人们不断要求它，我们决定屈服于大众的需求。

###Does the SLF4J logging API support I18N (internationalization)?(SLF4J日志API是否支持I18N（国际化）？)
是的，从版本1.5.9开始，SLF4J附带一个名为org.slf4j.cal10n的软件包，它将本地化/国际化日志记录支持添加为基于[CAL10N API](http://cal10n.qos.ch/)的层面。

##Is it possible to retrieve loggers without going through the static methods in `LoggerFactory`?(是否有可能检索日志记录器，而无需通过LoggerFactory中的静态方法？)
是。 `LoggerFactory` 本质上是一个[ILoggerFactory](http://www.slf4j.org/xref/org/slf4j/ILoggerFactory.html)实例的包装器。 正在使用的`ILoggerFactory`实例根据SLF4J框架的静态绑定约定来确定。 有关详细信息，请参阅`LoggerFactory`中的getSingleton（）方法。

但是，没有什么能阻止您使用自己的`ILoggerFactory`实例。 请注意，您还可以通过调用`LoggerFactory.getILoggerFactory()`方法获取`LoggerFactory`类正在使用的`ILoggerFactory`的引用。

因此，如果SLF4J绑定约定不符合您的需要，或者如果您需要额外的灵活性，那么请考虑使用`ILoggerFactory`接口作为发明您自己的日志API的替代方法。

###In the presence of an exception/throwable, is it possible to parameterize a logging statement?(在出现exception/throwable时，是否可以参数化一个日志语句？)
是的，从SLF4J 1.6.0，但不是在以前的版本。 Tha SLF4J API支持在存在异常时进行参数化，假设异常是最后一个参数。 例，
```
String s = "Hello world";
try {
  Integer i = Integer.valueOf(s);
} catch (NumberFormatException e) {
  logger.error("Failed to format {}", s, e);
}
```
将按预期打印NumberFormatException及其堆栈跟踪。 [java编译器将调用错误方法，该方法接受一个String和两个Object参数](http://www.slf4j.org/apidocs/org/slf4j/Logger.html#error%28java.lang.String,%20java.lang.Object,%20java.lang.Object%29)。 SLF4J根据程序员的最可能的意图，将NumberFormatException实例解释为throwable而不是一个未使用的Object参数。 在1.6.0之前的SLF4J版本中，NumberFormatException实例被忽略。

如果异常不是最后一个参数，它将被视为一个普通对象，并且不会打印它的堆栈跟踪。 然而，这种情况在实践中不应该发生。

##Implementing the SLF4J API
###How do I make my logging framework SLF4J compatible?(如何使我的日志框架SLF4J兼容？)
添加对SLF4J的支持是令人惊讶的容易。基本上，你处理一个现有的绑定和定制它一点（如下面解释）做的伎俩。

假设您的日志记录系统有一个记录器的概念，称为MyLogger，您需要为MyLogger提供一个适配器到org.slf4j.Logger接口。有关适配器的示例，请参阅`slf4j-jcl`，`slf4j-jdk14`和`slf4j-log4j12`模块。

一旦你写了一个合适的适配器，叫做`MyLoggerAdapter`，你需要提供一个实现`org.slf4j.ILoggerFactory`接口的工厂类。这个工厂应该返回实例`MyLoggerAdapter`。让`MyLoggerFactory`是你的工厂类的名字。

一旦你有适配器，即`MyLoggerAdapter`,一个工厂，即`MyLoggerFactory`，最后剩下的步骤是修改`StaticLoggerBinder`类，以便它返回一个新的`MyLoggerFactory`实例。您还需要修改`loggerFactoryClassStr`变量。

对于Marker或MDC支持，您可以使用现有的NOP实现之一。

总之，要为您的日志记录系统创建SLF4J绑定，请按照下列步骤操作：
1. start with a copy of an existing module(从现有模块的副本开始),
2. create an adapter between your logging system and org.slf4j.Logger interface(在日志系统和org.slf4j.Logger接口之间创建一个适配器),
3. create a factory for the adapter created in the previous step,(为上一步中创建的适配器创建工厂)
4. modify StaticLoggerBinder class to use the factory you created in the previous step(修改StaticLoggerBinder类以使用您在上一步中创建的工厂)

###How can my logging system add support for the Marker interface?(我的日志记录系统如何添加对Marker接口的支持？)
标记构成了一个革命性的概念，它由logback支持，但不支持其他现有的日志系统。 因此，允许符合SLF4J的日志系统忽略用户传递的标记数据。

然而，即使标记数据可以被忽略，仍然允许用户指定标记数据。 否则，用户将无法在支持标记的日志记录系统和不支持标记的日志记录系统之间切换。

MarkerIgnoringBase类可以作为缺少标记支持的日志系统的适配器或本机实现的基础。 在MarkerIgnoringBase中，采用标记数据的方法仅调用没有Marker参数的相应方法，丢弃作为参数传递的任何标记数据。 您的SLF4J适配器可以扩展MarkerIgnoringBase以快速实现org.slf4j.Logger中以Marker作为第一个参数的方法。

###How does SLF4J's version check mechanism work?(SLF4J的版本检查机制如何工作？)

SLF4J API在其初始化期间执行的版本检查是一个可选过程。符合SLF4J的实现可以选择不参与，在这种情况下，将不执行版本检查。

但是，如果SLF4J实现决定参与，则需要在其StaticLoggerBinder类的副本中声明一个名为REQUESTED_API_VERSION的变量。这个变量的值应该等于它编译的slf4j-api.jar的版本。如果实现升级到较新版本的slf4j-api，您还需要更新REQUESTED_API_VERSION的值。

对于每个版本，SLF4J API维护兼容版本的列表。只有在兼容性列表中找不到所请求的版本时，SLF4J才会发出版本不匹配警告。因此，即使您的SLF4J绑定具有与SLF4J不同的发布计划，假设您更新每6到12个月使用的SLF4J版本，您仍然可以参与版本检查，而不会导致不匹配警告。例如，logback具有不同的发布计划，但仍参与版本检查。

从SLF4J 1.5.5开始，在SLF4J分布中运输的所有结合，例如。 slf4j-log4j12，slf4j-simple和slf4j-jdk14声明REQUESTED_API_VERSION字段，其值等于它们的SLF4J版本。因此，例如如果slf4j-simple-1.5.8.jar与slf4j-api-1.6.0.jar混合，假定1.5.8不在SLF4J版本1.6.x的兼容性列表上，则版本不匹配将发出警告。

请注意，1.5.5之前的SLF4J版本没有版本检查机制。只有slf4j-api-1.5.5.jar和更高版本可以发出版本不匹配警告。

##General questions about logging
###Should Logger members of a class be declared as static?(应该将类的Logger成员声明为静态吗？)
我们曾经建议将clogger成员声明为实例变量，而不是static。 经过进一步分析，我们不再推荐一种方法。

这里总结了每种方法的利弊。
| 将Logger声明为静态的优点 | 将Logger声明为静态的缺点 |
|--- |--- |
|1.共同惯用语法;2.减少CPU开销：在托管类初始化时，检索和分配记录器一次;3.减少内存开销：logger声明每个类将消耗一个引用 | 对于应用程序之间共享的库，不可能利用存储库选择器。 应该注意，如果SLF4J绑定和底层API随每个应用程序一起提供（不在应用程序之间共享），那么每个应用程序仍将有自己的日志记录环境。没有IOC友好 |

| 声明日志记录器作为实例变量的优点 | 声明日志记录器作为实例变量的缺点|
|--- |--- |
|可以利用存储库选择器，甚至对应用程序之间共享的库。 但是，存储库选择器仅在基础日志记录系统为logback-classic时有效。 存储库选择器不适用于SLF4J + log4j组合。IOC友好 | 比将记录器声明为静态变量更常见的语义,更高的CPU开销：检索并为托管类的每个实例分配记录器；更高的内存开销：记录器声明将为托管类的每个实例使用一个引用 |

##Explanation(说明)
静态记录器成员为类的所有实例花费一个变量引用，而实例记录器成员将花费类的每个实例的变量引用。对于简单类实例化数千次可能有明显的区别。

然而，更新的日志系统（例如log4j或logback）支持在应用程序服务器中运行的每个应用程序的不同的记录器上下文。因此，即使log4j.jar或logback-classic.jar的单个副本部署在服务器中，日志记录系统也能够区分应用程序，并为每个应用程序提供一个不同的日志记录环境。

更具体地说，每次通过调用LoggerFactory.getLogger()方法检索日志记录器时，底层日志记录系统将返回适合当前应用程序的实例。请注意，在同一应用程序中，通过给定名称检索记录器将总是返回相同的记录器。对于给定的名称，将仅为不同的应用程序返回不同的记录器。

如果记录器是静态的，那么它只会在主机类加载到内存时检索一次。如果托管类只在一个应用程序中使用，则没有太多的关注。然而，如果托管类在多个应用程序之间共享，则共享类的所有实例将登录到应用程序的上下文中，首先将共享类加载到内存中 - 几乎不是用户期望的行为。

不幸的是，对于SLF4J API的非本地实现，即使用slf4j-log4j12，log4j的存储库选择器将不能正确地完成其工作，因为非本地SLF4J绑定的slf4j-log4j12将在地图中存储记录器实例，短路上下文相关的记录器检索。对于原生SLF4J实现，例如logback-classic，存储库选择器将按预期工作。

Apache Commons 文库包含一篇涉及同一问题的[信息性文章](http://wiki.apache.org/jakarta-commons/Logging/StaticLog)。

###Logger serialization(日志序列化)
与静态变量相反，实例变量在默认情况下是序列化的。 从SLF4J版本1.5.3开始，记录器实例在序列化中存活。 因此，主机类的序列化不再需要任何特殊的操作，即使当日志记录器被声明为实例变量时。 在以前的版本中，需要在主机类中将logger实例声明为transient。

###Summary概要
总之，将记录器成员声明为静态变量需要更少的CPU时间，并且内存占用量略小。 另一方面，将logger成员声明为实例变量需要更多的CPU时间，并且内存开销略高。 但是，实例变量使得可以为每个应用程序创建一个不同的记录器环境，即使是在共享库中声明的记录器。 也许比以前提到的更重要的考虑，实例变量是IOC友好的，而静态变量不是。
另见[commons-logging wiki中的相关讨论](http://wiki.apache.org/jakarta-commons/Logging/StaticLog)。

###Is there a recommended idiom for declaring a logger in a class?(在类中声明一个记录器是否有一个推荐的语法？)
以下是推荐的记录器声明语法。 由于上面解释的原因，留给用户来确定记录器是否被声明为静态变量。
```
package some.package;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyClass {
  final (static) Logger logger = LoggerFactory.getLogger(MyClass.class);
  ... etc
}
```
不幸的是，由于托管类的名称是记录器声明的一部分，上述记录器声明成语不能防止类之间的剪切和粘贴。
